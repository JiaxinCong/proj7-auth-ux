# Laptop Service

import os
from flask import Flask, jsonify, request, render_template, redirect, url_for, flash, session
from flask_restful import Resource, Api, request, reqparse
from pymongo import MongoClient
from wtforms import Form, BooleanField, StringField, PasswordField
from wtforms.validators import DataRequired
from password import hash_password, verify_password
from flask_login import (LoginManager, current_user, login_required,
                            login_user, logout_user, UserMixin,
                            confirm_login, fresh_login_required)



# Instantiate the app
app = Flask(__name__)
api = Api(app)

client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'], 27017)

db = client.tododb
db2 = client.user

@app.route('/')
def index():
    return render_template("index.html")

@app.route('/found')
def userfound():
    return render_template("found.html"),400

@app.route('/nouser')
def nouser():
    return render_template("nouser.html"),400

@app.route('/logout')
def out():
    return render_template("out.html")

#reference: https://www.youtube.com/watch?v=addnlzdSQs4&t=987s

class RegisterForm(Form):
    username = StringField('username', validators=[DataRequired()])
    password = PasswordField('password', validators=[DataRequired()])

@app.route('/register', methods=['GET','POST'])
def register():
    form = RegisterForm(request.form)
    if request.method == 'POST' and form.validate():
        username = form.username.data
        password = hash_password(form.password.data)
        id = hash_password(username)
        userresult = db2.user.find_one({"user":username})
        if userresult != None:
            return userfound()
        else:
            db2.user.insert({"id": id, "user":username,"password":password})

    return render_template('register.html',form=form)

class LoginForm(Form):
    username = StringField('username', validators=[DataRequired()])
    password = PasswordField('password', validators=[DataRequired()])
    remember = BooleanField('remember', validators=[DataRequired()])

@app.route('/login', methods=['GET','POST'])
def login():
    form = LoginForm(request.form)
    if request.method == 'POST' and form.validate():
        username = form.username.data
        useresult = db2.user.find_one({"user":username})
        password = form.password.data
        if useresult == None:
            return nouser()
        else:
            if verify_password(password, useresult["password"]):
                return redirect(url_for('index'))

    return render_template('login.html', form=form)

@app.route("/logout")
@login_required
def logout():
    logout_user()
    return out()

@app.route("/token")



class Laptop(Resource):
    def get(self):
        return {
            'Laptops': ['Mac OS', 'Dell',
                        'Windozzee',
                        'Yet another laptop!',
                        'Yet yet another laptop!'
                        ]
    }

class Timeall(Resource):
    def get(self):
        _items = db.tododb.find()
        items = [item for item in _items]
        opentime = [item["open"] for item in items]
        closetime = [item["close"] for item in items]
        return {'Opentime':opentime, 'Closetime': closetime}

class Openall(Resource):
    def get(self):
        _items = db.tododb.find()
        items = [item for item in _items]
        opentime = [item["open"] for item in items]
        return {'Opentime': opentime}

class Closeall(Resource):
    def get(self):
        _items = db.tododb.find()
        items = [item for item in _items]
        closetime = [item["close"] for item in items]
        return {'Closetime':closetime}

class Timeallformat(Resource):
    def get(self,format):
        _items = db.tododb.find()
        #return csv format
        if format == 'csv':
            items = [item for item in _items]
            opentime = [item["open"] for item in items]
            closetime = [item["close"] for item in items]
            open = ""
            close = ""
            for s in opentime:
                open = s + ', ' + open
            for x in closetime:
                close = x + ', ' + close
            return 'Opentime: '+ open + ' Closetime: ' + close
        #return json format
        elif format == 'json':
            items = [item for item in _items]
            opentime = [item["open"] for item in items]
            closetime = [item["close"] for item in items]
            return {'Opentime':opentime, 'Closetime': closetime}

class Openallformat(Resource):
    def get(self, format):
        _items = db.tododb.find()
        #return csv
        n = request.args.get('top', type = int)
        if n != None:
            if format == 'csv':
                items = [item for item in _items]
                opentime = [item["open"] for item in items]
                open = ""
                i = 0
                while i<n:
                    open = opentime[i] + ', ' + open
                    i+=1
                return open
            #return json format
            elif format == 'json':
                items = [item for item in _items]
                i = 0
                opentime = []
                while i < n:
                    opentime.append(items[i]['open'])
                    i += 1
                return {'opentime': opentime}
        if format == 'csv':
            items = [item for item in _items]
            opentime = [item["open"] for item in items]
            open = ""
            for s in opentime:
                open = s + ', ' + open
            return open
        #return json format
        elif format == 'json':
            items = [item for item in _items]
            opentime = [item["open"] for item in items]
            return {'Opentime': opentime}

class Closeallformat(Resource):
    def get(self,format):
        _items = db.tododb.find()
        #return csv format
        n = request.args.get('top', type = int)
        if n != None:
            if format == 'csv':
                items = [item for item in _items]
                closetime = [item["close"] for item in items]
                close = ""
                i = 0
                while i<n:
                    close = closetime[i] + ', ' + close
                    i+=1
                return close
            #return json format
            elif format == 'json':
                items = [item for item in _items]
                i = 0
                closetime = []
                while i < n:
                    closetime.append(items[i]['close'])
                    i += 1
                return {'Closetime': closetime}
        if format == 'csv':
            items = [item for item in _items]
            closetime = [item["close"] for item in items]
            close = ""
            for s in closetime:
                close = s + ', ' + close
            return close
        #return json format
        elif format == 'json':
            items = [item for item in _items]
            closetime = [item["close"] for item in items]
            return {'Closetime': closetime}





# Create routes
# Another way, without decorators
api.add_resource(Laptop, '/')
api.add_resource(Timeall,'/listAll')
api.add_resource(Openall,'/listOpenOnly')
api.add_resource(Closeall,'/listCloseOnly')
api.add_resource(Timeallformat,'/listAll/<format>')
api.add_resource(Openallformat,'/listOpenOnly/<format>')
api.add_resource(Closeallformat,'/listCloseOnly/<format>')


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=80, debug=True)
